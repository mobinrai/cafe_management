require('./bootstrap');

window.Vue=require('vue');
/**
 * This is a global vue file 
 */
Vue.component('card-component',require('./components/Card.vue'));
/**
 * This is a required/partial vue files 
 */
Vue.component('menu-container',require('./modules/menu/MenuContainer.vue').default);

const app = new Vue({
    el: '#app'
});