<?php

namespace App\Services;
use \App\Menu;

class MenuServices
{
    public function getMenuWithCategory(array $resto_id)
    {
        $category=Menu::whereIn('resto_id',$resto_id)->get()->groupBy('category.name');

        return $category;
    }
}