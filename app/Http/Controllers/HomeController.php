<?php

namespace App\Http\Controllers;

use App\Services\MenuServices;
use Illuminate\Http\Request;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(MenuServices $services)
    {
        $resto_id   = ['1'];
        $menu       = $services->getMenuWithCategory($resto_id);
        return view('home',['menus'=>$menu]);
    }
}
